##
## Build
##
FROM golang:1.16-buster AS build

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .
COPY .env.example .env

RUN go build -o /be-sidat

##
## Deploy
##
FROM gcr.io/distroless/base-debian10

WORKDIR /

COPY --from=build /be-sidat /be-sidat
COPY --from=build /app/.env /.env

EXPOSE 8000

USER nonroot:nonroot

ENTRYPOINT ["/be-sidat"]