package controllers

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"net/url"
	"os"
	"sidat-bispro/database"
	"sidat-bispro/models"
	"sidat-bispro/models/reqresp"
	"sidat-bispro/util"
	"strconv"
	"strings"
)

// AllProdukHukum ProdukHukum list
// @Summary ProdukHukum list
// @Description ProdukHukum list
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param jenis_perundangan_id query int false "Jenis Perundangan ID"
// @param nama query string false "Nama Produk Hukum"
// @param tahun query int false "Tahun Produk Hukum"
// @param keterangan query string false "Keterangan"
// @param sort query string false "Sort by"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /produk-hukum [get]
func AllProdukHukum(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	jenisPerundanganId, _ := strconv.Atoi(c.Query("jenis_perundangan_id", "0"))
	nama := c.Query("nama", "")
	tahun, _ := strconv.Atoi(c.Query("tahun", "0"))
	keterangan := c.Query("keterangan", "")
	sortStr := c.Query("sort", "")

	var records []models.ProdukHukum

	baseQ := database.DB.Preload(clause.Associations)
	if jenisPerundanganId > 0 {
		baseQ = baseQ.Where("jenis_perundangan_id = ?", jenisPerundanganId)
	}

	if nama != "" {
		baseQ = baseQ.Where("UPPER(nama) LIKE ?", "%"+strings.ToUpper(nama)+"%")
	}

	if tahun > 0 {
		baseQ = baseQ.Where("tahun = ?", tahun)
	}

	if keterangan == "proleg" {
		baseQ = baseQ.Where("keterangan = ?", "proleg")
	} else if keterangan == "non-proleg" {
		baseQ = baseQ.Not("keterangan = ?", "proleg")
	}

	if sortStr != "" {
		sortParsed, err := url.PathUnescape(sortStr)
		if err != nil {
			return err
		}
		var s []reqresp.SortRequest
		err = json.Unmarshal([]byte(sortParsed), &s)
		if err != nil {
			return err
		}
		if s[0].Desc {
			baseQ = baseQ.Order(s[0].Selector + " desc")
		} else {
			baseQ = baseQ.Order(s[0].Selector)
		}
	}

	//execute query
	baseQ.Offset(offset).Limit(limit).Find(&records)

	baseQ.Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// GetProdukHukumStats ProdukHukum stats
// @Summary ProdukHukum stats
// @Description ProdukHukum stats
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @Success 200 {object} reqresp.SuccessResponse
// @Router /produk-hukum/stats [get]
func GetProdukHukumStats(c *fiber.Ctx) error {

	var records []reqresp.StatsResponse

	baseQ := database.DB.Table("produk_hukum k").Select("jp.name as name, count(*) as total")
	baseQ = baseQ.Joins("join jenis_perundangan jp on jp.id = k.jenis_perundangan_id").Group("jp.name")

	//execute query
	baseQ.Find(&records)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success calculating ProdukHukum statistic",
		Data:    records,
	})
}

// CreateProdukHukum Create a ProdukHukum
// @Summary Create a ProdukHukum
// @Description Create a ProdukHukum
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.ProdukHukum true "ProdukHukum data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /produk-hukum [post]
func CreateProdukHukum(c *fiber.Ctx) error {
	var record models.ProdukHukum

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "produk_hukum", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetProdukHukum Get single ProdukHukum data
// @Summary Get single ProdukHukum data
// @Description Get single ProdukHukum data
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param id path int true "ProdukHukum ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /produk-hukum/{id} [get]
func GetProdukHukum(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.ProdukHukum{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateProdukHukum Update a ProdukHukum
// @Summary Update a ProdukHukum
// @Description Update a ProdukHukum
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "ProdukHukum ID"
// @param data body models.ProdukHukum true "ProdukHukum data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /produk-hukum/{id} [put]
func UpdateProdukHukum(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.ProdukHukum{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.ProdukHukum
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "produk_hukum", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteProdukHukum Delete a ProdukHukum
// @Summary Delete a ProdukHukum
// @Description Delete a ProdukHukum
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "ProdukHukum ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /produk-hukum/{id} [delete]
func DeleteProdukHukum(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.ProdukHukum{Id: uint(id)}

	var oldData models.ProdukHukum
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// delete uploaded file
	_ = os.Remove("./public/" + record.ProdukPath)
	//if err != nil {
	//	return c.Status(fiber.StatusInternalServerError).JSON(err)
	//}

	// send to log
	util.SendToAudit(c, "produk_hukum", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "ProdukHukum record deleted",
		Data:    record,
	})
}
