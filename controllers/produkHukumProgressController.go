package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm/clause"
	"math"
	"os"
	"sidat-bispro/database"
	"sidat-bispro/models"
	"sidat-bispro/models/reqresp"
	"sidat-bispro/util"
	"strconv"
)

// AllProdukHukumProgress ProdukHukumProgress list
// @Summary ProdukHukumProgress list
// @Description ProdukHukumProgress list
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param produk_hukum_id query int true "Advokasi ID"
// @param skip query int false "Number of records to skip"
// @param take query int false "Number of records"
// @Success 200 {object} reqresp.DxDatagridResponse
// @Router /produk-hukum-progress [get]
func AllProdukHukumProgress(c *fiber.Ctx) error {
	limit, _ := strconv.Atoi(c.Query("take", "10"))
	offset, _ := strconv.Atoi(c.Query("skip", "0"))
	page := math.Ceil(float64(offset/limit)) + 1
	var total int64

	produkHukumId, _ := strconv.Atoi(c.Query("produk_hukum_id", "0"))

	var records []models.ProdukHukumProgress

	database.DB.Preload(clause.Associations).Where("produk_hukum_id = ?", produkHukumId).Offset(offset).Limit(limit).Find(&records)

	database.DB.Where("produk_hukum_id = ?", produkHukumId).Model(&models.ProdukHukumProgress{}).Count(&total)

	lastPage := math.Ceil(float64(int(total) / limit))
	if lastPage <= 0 {
		lastPage = 1
	}

	return c.JSON(&reqresp.DxDatagridResponse{
		Data:       records,
		TotalCount: int(total),
		Summary:    nil,
		Meta: fiber.Map{
			"total":     total,
			"page":      page,
			"last_page": lastPage,
		},
	})
}

// CreateProdukHukumProgress Create a ProdukHukumProgress
// @Summary Create a ProdukHukumProgress
// @Description Create a ProdukHukumProgress
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param data body models.ProdukHukumProgress true "ProdukHukumProgress data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /produk-hukum-progress [post]
func CreateProdukHukumProgress(c *fiber.Ctx) error {
	var record models.ProdukHukumProgress

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	result := database.DB.Preload(clause.Associations).Create(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusBadRequest).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to create record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "produk_hukum_progress", "insert", nil, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success creating record",
		Data:    record,
	})
}

// GetProdukHukumProgress Get single ProdukHukumProgress data
// @Summary Get single ProdukHukumProgress data
// @Description Get single ProdukHukumProgress data
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param id path int true "ProdukHukumProgress ID"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 404 {object} reqresp.ErrorResponse
// @Router /produk-hukum-progress/{id} [get]
func GetProdukHukumProgress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.ProdukHukumProgress{Id: uint(id)}

	result := database.DB.Preload(clause.Associations).Find(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusNotFound).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Record not found",
		})
	}

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success getting a record",
		Data:    record,
	})
}

// UpdateProdukHukumProgress Update a ProdukHukumProgress
// @Summary Update a ProdukHukumProgress
// @Description Update a ProdukHukumProgress
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "ProdukHukumProgress ID"
// @param data body models.ProdukHukumProgress true "ProdukHukumProgress data"
// @Success 200 {object} reqresp.SuccessResponse
// @Error 400 {object} reqresp.ErrorResponse
// @Router /produk-hukum-progress/{id} [put]
func UpdateProdukHukumProgress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.ProdukHukumProgress{Id: uint(id)}

	if err := c.BodyParser(&record); err != nil {
		return err
	}

	var oldData models.ProdukHukumProgress
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Model(&record).Preload(clause.Associations).Updates(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to update record: " + result.Error.Error(),
		})
	}

	// send to log
	util.SendToAudit(c, "produk_hukum_progress", "update", &oldData, &record)

	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "Success updating record",
		Data:    record,
	})
}

// DeleteProdukHukumProgress Delete a ProdukHukumProgress
// @Summary Delete a ProdukHukumProgress
// @Description Delete a ProdukHukumProgress
// @Tags Produk Hukum
// @Accept json
// @Produce json
// @param Authorization header string true "Authorization"
// @param id path int true "ProdukHukumProgress ID"
// @Success 204 {object} reqresp.SuccessResponse
// @Error 500 {object} reqresp.ErrorResponse
// @Router /produk-hukum-progress/{id} [delete]
func DeleteProdukHukumProgress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	record := models.ProdukHukumProgress{Id: uint(id)}

	var oldData models.ProdukHukumProgress
	database.DB.Where("id = ?", id).First(&oldData)

	result := database.DB.Delete(&record)
	if result.Error != nil || result.RowsAffected <= 0 {
		return c.Status(fiber.StatusInternalServerError).JSON(&reqresp.ErrorResponse{
			Status:  "error",
			Message: "Failed to delete record: " + result.Error.Error(),
		})
	}

	// delete uploaded file
	_ = os.Remove("./public/" + record.Path)
	//if err != nil {
	//	return c.Status(fiber.StatusInternalServerError).JSON(err)
	//}

	// send to log
	util.SendToAudit(c, "produk_hukum_progress", "delete", &oldData, nil)

	c.Status(fiber.StatusNoContent)
	return c.JSON(&reqresp.SuccessResponse{
		Status:  "success",
		Message: "ProdukHukumProgress record deleted",
		Data:    record,
	})
}
