package models

import (
	"time"
)

type Advokasi struct {
	Id           uint               `json:"id" gorm:"primaryKey"`
	CreatedAt    time.Time          `json:"created_at"`
	UpdatedAt    time.Time          `json:"updated_at"`
	Perkara      string             `json:"perkara"`
	KasusHukum   string             `json:"kasus_hukum"`
	Materi       string             `json:"materi"`
	DirektoratId uint               `json:"direktorat_id"`
	Direktorat   Direktorat         `json:"direktorat" gorm:"foreignKey:DirektoratId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	AddNote      string             `json:"add_note"`
	Progress     []AdvokasiProgress `json:"progress"`
}

func (a Advokasi) TableName() string {
	return "bispro.advokasi"
}