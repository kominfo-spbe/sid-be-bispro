package models

import (
	"time"
)

type Direktorat struct {
	Id        uint            `json:"id" gorm:"primaryKey"`
	CreatedAt time.Time       `json:"created_at"`
	UpdatedAt time.Time       `json:"updated_at"`
	Code      string          `json:"code" gorm:"unique"`
	Name      string          `json:"name" gorm:"unique"`
	Subdirs   []Subdirektorat `json:"subdirektorat"`
}

func (d Direktorat) TableName() string {
	return "master_ref.direktorat"
}
