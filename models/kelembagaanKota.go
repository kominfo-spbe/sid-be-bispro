package models

import (
	"time"
)

type KelembagaanKota struct {
	Id                  uint              `json:"id" gorm:"primaryKey"`
	CreatedAt           time.Time         `json:"created_at"`
	UpdatedAt           time.Time         `json:"updated_at"`
	KabkotaId           uint              `json:"kabkota_id"`
	Kabkota             Kabkota           `json:"kabkota" gorm:"foreignKey:KabkotaId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	BentukKelembagaanId uint              `json:"bentuk_kelembagaan_id"`
	BentukKelembagaan   BentukKelembagaan `json:"bentuk_kelembagaan" gorm:"foreignKey:BentukKelembagaanId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Nomenklatur         string            `json:"nomenklatur"`
	Alamat              string            `json:"alamat"`
}

func (k KelembagaanKota) TableName() string {
	return "bispro.kelembagaan_kota"
}
