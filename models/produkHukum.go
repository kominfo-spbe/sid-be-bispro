package models

import (
	"github.com/jackc/pgtype"
	"time"
)

type ProdukHukum struct {
	Id                 uint             `json:"id" gorm:"primaryKey"`
	CreatedAt          time.Time        `json:"created_at"`
	UpdatedAt          time.Time        `json:"updated_at"`
	DirektoratId       uint             `json:"direktorat_id"`
	Direktorat         Direktorat       `json:"direktorat" gorm:"foreignKey:DirektoratId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	SubdirektoratId    uint             `json:"subdirektorat_id"`
	Subdirektorat      Direktorat       `json:"subdirektorat" gorm:"foreignKey:SubdirektoratId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	JenisPerundanganId uint             `json:"jenis_perundangan_id"`
	JenisPerundangan   JenisPerundangan `json:"jenis_perundangan" gorm:"foreignKey:DirektoratId;constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	Nama               string           `json:"nama"`
	Tahun              int              `json:"tahun"`
	Target             pgtype.Date      `json:"target"`
	Progress           string           `json:"progress"`
	TanggalProgress    pgtype.Date      `json:"tanggal_progress"`
	Pelaporan          string           `json:"pelaporan"`
	Keterangan         string           `json:"keterangan"`
	ProdukPath         string           `json:"produk_path"`
	AddNote            string           `json:"add_note"`
}

func (p ProdukHukum) TableName() string {
	return "bispro.produk_hukum"
}
