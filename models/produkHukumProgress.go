package models

import (
	"github.com/jackc/pgtype"
	"time"
)

type ProdukHukumProgress struct {
	Id            uint           `json:"id" gorm:"primaryKey"`
	CreatedAt     time.Time      `json:"created_at"`
	UpdatedAt     time.Time      `json:"updated_at"`
	ProdukHukumId uint           `json:"produk_hukum_id"`
	ProdukHukum   ProdukHukum    `json:"produk_hukum" gorm:"foreignKey:ProdukHukumId;constraint:OnUpdate:CASCADE,OnDelete:CASCADE;"`
	Judul         string         `json:"judul"`
	Laporan       string         `json:"laporan"`
	Tanggal       pgtype.Date    `json:"tanggal"`
	Path          string         `json:"path"`
}

func (p ProdukHukumProgress) TableName() string {
	return "bispro.produk_hukum_progress"
}
