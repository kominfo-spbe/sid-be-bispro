package routes

import (
	"github.com/gofiber/fiber/v2"
	jwtware "github.com/gofiber/jwt/v3"
	"os"
	"sidat-bispro/controllers"
)

func Setup(app *fiber.App) {

	app.Get("/api/advokasi", controllers.AllAdvokasi)
	app.Get("/api/advokasi/:id", controllers.GetAdvokasi)
	app.Get("/api/advokasi-progress", controllers.AllAdvokasiProgress)
	app.Get("/api/advokasi-progress/:id", controllers.GetAdvokasiProgress)

	app.Get("/api/dokumentasi", controllers.AllDokumentasi)
	app.Get("/api/dokumentasi/stats", controllers.GetDokumentasiStats)
	app.Get("/api/dokumentasi/increase-view/:id", controllers.GetDokumentasiIncreaseView)
	app.Get("/api/dokumentasi/increase-download/:id", controllers.GetDokumentasiIncreaseDownload)
	app.Get("/api/dokumentasi/:id", controllers.GetDokumentasi)

	app.Get("/api/produk-hukum", controllers.AllProdukHukum)
	app.Get("/api/produk-hukum/stats", controllers.GetProdukHukumStats)
	app.Get("/api/produk-hukum/:id", controllers.GetProdukHukum)

	app.Get("/api/produk-hukum-progress", controllers.AllProdukHukumProgress)
	app.Get("/api/produk-hukum-progress/:id", controllers.GetProdukHukumProgress)

	app.Get("/api/kelembagaan-provinsi", controllers.AllKelembagaanProvinsi)
	app.Get("/api/kelembagaan-provinsi/stats", controllers.GetStatsKelembagaanProvinsi)
	app.Get("/api/kelembagaan-provinsi/:id", controllers.GetKelembagaanProvinsi)

	app.Get("/api/kelembagaan-kota", controllers.AllKelembagaanKota)
	app.Get("/api/kelembagaan-kota/stats", controllers.GetStatsKelembagaanKota)
	app.Get("/api/kelembagaan-kota/:id", controllers.GetKelembagaanKota)

	// must be logged-in
	//app.Use(middleware.IsAuthenticated)
	app.Use(jwtware.New(jwtware.Config{
		SigningKey: []byte(os.Getenv("TOKEN_SECRET")),
	}))

	app.Post("/api/advokasi", controllers.CreateAdvokasi)
	app.Put("/api/advokasi/:id", controllers.UpdateAdvokasi)
	app.Delete("/api/advokasi/:id", controllers.DeleteAdvokasi)

	app.Post("/api/advokasi-progress", controllers.CreateAdvokasiProgress)
	app.Put("/api/advokasi-progress/:id", controllers.UpdateAdvokasiProgress)
	app.Delete("/api/advokasi-progress/:id", controllers.DeleteAdvokasiProgress)

	app.Post("/api/dokumentasi", controllers.CreateDokumentasi)
	app.Put("/api/dokumentasi/:id", controllers.UpdateDokumentasi)
	app.Delete("/api/dokumentasi/:id", controllers.DeleteDokumentasi)

	app.Post("/api/produk-hukum", controllers.CreateProdukHukum)
	app.Put("/api/produk-hukum/:id", controllers.UpdateProdukHukum)
	app.Delete("/api/produk-hukum/:id", controllers.DeleteProdukHukum)

	app.Post("/api/produk-hukum-progress", controllers.CreateProdukHukumProgress)
	app.Put("/api/produk-hukum-progress/:id", controllers.UpdateProdukHukumProgress)
	app.Delete("/api/produk-hukum-progress/:id", controllers.DeleteProdukHukumProgress)

	app.Post("/api/kelembagaan-provinsi", controllers.CreateKelembagaanProvinsi)
	app.Put("/api/kelembagaan-provinsi/:id", controllers.UpdateKelembagaanProvinsi)
	app.Delete("/api/kelembagaan-provinsi/:id", controllers.DeleteKelembagaanProvinsi)

	app.Post("/api/kelembagaan-kota", controllers.CreateKelembagaanKota)
	app.Put("/api/kelembagaan-kota/:id", controllers.UpdateKelembagaanKota)
	app.Delete("/api/kelembagaan-kota/:id", controllers.DeleteKelembagaanKota)
}
